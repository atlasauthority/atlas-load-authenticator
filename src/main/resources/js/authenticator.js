(function($) {
    var url = AJS.contextPath() + '/rest/load-auth/1.0/';

    $(document).ready(function() {

        $('#filteringEnabled').change(function() {
            if ($('#filteringEnabled').attr('checked')) {
                $('#ipaddresses').removeProp('disabled');
            } else {
                $('#ipaddresses').prop('disabled', 'true');
            }
        });

        $.ajax({
            url: url,
            dataType: 'json'
        }).done(function(model) {
            $('#ipaddresses').val(model.whitelistedIps);
            if(model.debugOn) {
                $('#debug').prop('checked', true);
            }
            if(model.filteringEnabled) {
                $('#filteringEnabled').prop('checked', true);
            } else {
                $('#ipaddresses').prop('disabled', 'true');
            }
        });

        $('#ipForm').submit(function (e) {
            e.preventDefault();
            updateConfig();
        })
    });
})(AJS.$ || jQuery);

function updateConfig() {
    var dbg = false;
    if (AJS.$('#debug').attr('checked')) {
        dbg = true;
    }
    var filteringEnabled = false;
    if (AJS.$('#filteringEnabled').attr('checked')) {
        filteringEnabled = true;
    }
    AJS.$.ajax({
        url: AJS.contextPath() + '/rest/load-auth/1.0/',
        type: 'PUT',
        contentType: 'application/json',
        data: '{"whitelistedIps": "' + AJS.$('#ipaddresses').attr('value') + '","debugOn": ' + dbg + ',"filteringEnabled":' + filteringEnabled + '}',
        processData: false
    }).done(function() {
        AJS.messages.success('#messages-area', {
            title: 'Success!',
            body: '<p>IP addresses were saved successfully.</p>'
        })
    });
}
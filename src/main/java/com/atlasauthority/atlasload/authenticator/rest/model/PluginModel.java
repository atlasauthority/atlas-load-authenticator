package com.atlasauthority.atlasload.authenticator.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Andrew Kharchenko
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PluginModel {

    @XmlElement
    private String whitelistedIps;
    private Boolean debugOn = false;
    private Boolean filteringEnabled = true;

    public String getWhitelistedIps() {
        return whitelistedIps;
    }

    public PluginModel setWhitelistedIps(String whitelistedIps) {
        this.whitelistedIps = whitelistedIps;
        return this;
    }

    public Boolean getDebugOn() {
        return debugOn;
    }

    public PluginModel setDebugOn(Boolean debugOn) {
        this.debugOn = debugOn;
        return this;
    }

    public Boolean getFilteringEnabled() {
        return filteringEnabled;
    }

    public PluginModel setFilteringEnabled(Boolean filteringEnabled) {
        this.filteringEnabled = filteringEnabled;
        return this;
    }

    @Override
    public String toString() {
        return "PluginModel{" +
                "whitelistedIps='" + whitelistedIps + '\'' +
                ", debugOn=" + debugOn +
                ", filteringEnabled=" + filteringEnabled +
                '}';
    }
}

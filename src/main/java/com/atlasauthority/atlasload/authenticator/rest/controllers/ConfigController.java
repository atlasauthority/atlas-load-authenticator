package com.atlasauthority.atlasload.authenticator.rest.controllers;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.atlasauthority.atlasload.authenticator.rest.model.PluginModel;
import com.atlasauthority.atlasload.authenticator.utils.LogHandler;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;

/**
 * @author Andrew Kharchenko
 */
@Path("/")
@Scanned
public class ConfigController {
    private static final Logger LOG = LogHandler.getLogger(ConfigController.class);

    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private final TransactionTemplate transactionTemplate;

    @Inject
    public ConfigController(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,
                            TransactionTemplate transactionTemplate) {
        this.userManager = userManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getConfig() {
        if (accessDenied()) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok(transactionTemplate.execute(() -> {
            PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
            PluginModel model = new PluginModel();
            model.setWhitelistedIps((String) pluginSettings.get(PluginModel.class.getName() + ".whitelistedIPs"));
            model.setDebugOn(Boolean.valueOf((String) pluginSettings.get(PluginModel.class.getName() + ".debugOn")));

            String filteringEnabled = (String) pluginSettings.get(PluginModel.class.getName() + ".filteringEnabled");
            if (filteringEnabled == null) {
                filteringEnabled = "true";
            }
            model.setFilteringEnabled(Boolean.valueOf(filteringEnabled));
            if (LOG.isDebugEnabled()) {
                LOG.debug("Read plugin settings: " + model);
            }
            return model;
        })).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveConfig(PluginModel model) {
        if (accessDenied()) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        transactionTemplate.execute(() -> {
            if (model.getDebugOn()) {
                LogHandler.setDebugLogLevel();
            } else {
                LogHandler.restoreLogLevel();
            }
            if (LOG.isDebugEnabled()) {
                LOG.debug("Save plugin settings: " + model);
            }
            PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
            pluginSettings.put(PluginModel.class.getName() + ".whitelistedIPs", model.getWhitelistedIps());
            pluginSettings.put(PluginModel.class.getName() + ".debugOn", model.getDebugOn().toString());
            pluginSettings.put(PluginModel.class.getName() + ".filteringEnabled", model.getFilteringEnabled().toString());
            return null;
        });

        return Response.noContent().build();
    }

    private boolean accessDenied() {
        UserProfile remoteUser = userManager.getRemoteUser();
        return remoteUser == null || !userManager.isSystemAdmin(remoteUser.getUserKey());
    }
}

package com.atlasauthority.atlasload.authenticator.servlet;

import java.io.IOException;
import java.net.URI;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.TemplateRenderer;

/**
 * @author Andrew Kharchenko
 */
@Scanned
public class PluginConfigServlet extends HttpServlet {
    @ComponentImport
    private TemplateRenderer templateRenderer;
    @ComponentImport
    private UserManager userManager;
    @ComponentImport
    private LoginUriProvider loginUriProvider;

    @Inject
    public PluginConfigServlet(TemplateRenderer templateRenderer,
                               UserManager userManager,
                               LoginUriProvider loginUriProvider) {
        this.templateRenderer = templateRenderer;
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UserProfile remoteUser = userManager.getRemoteUser();
        if (remoteUser == null || !userManager.isSystemAdmin(remoteUser.getUserKey())) {
            response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
            return;
        }

        response.setContentType("text/html;charset=utf-8");
        templateRenderer.render("config.vm", response.getWriter());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}

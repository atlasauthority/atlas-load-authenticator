package com.atlasauthority.atlasload.authenticator.filter;

import static com.atlasauthority.atlasload.authenticator.api.AuthPluginHelper.JSESSIONID;
import static com.atlasauthority.atlasload.authenticator.api.AuthPluginHelper.USERNAME_CUSTOM_HEADER;

import java.io.IOException;
import java.util.Arrays;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.log4j.Logger;

import com.atlasauthority.atlasload.authenticator.api.AuthPluginHelper;
import com.atlasauthority.atlasload.authenticator.utils.LogHandler;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

/**
 * @author Andrew Kharchenko
 */
public class CleanupFilter implements Filter {
    private static final Logger LOG = LogHandler.getLogger(CleanupFilter.class);
    @Inject
    @ComponentImport
    private AuthPluginHelper authPluginHelper;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException,
            ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        boolean notWhitelisted = authPluginHelper.requestNotWhitelisted(request);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Request " + request.getRequestURI() + " is " + (notWhitelisted ? "NOT" : "") + " whitelisted");
        }
        if (notWhitelisted) {
            chain.doFilter(servletRequest, servletResponse);
            return;
        }
        String userName = request.getHeader(USERNAME_CUSTOM_HEADER);
        if (userName != null) {
            synchronized (userName) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Handling cookies for user " + userName);
                    LogHandler.debugRequest(LOG, request);
                }
                HttpServletRequestWrapper requestWrapper = new HttpServletRequestWrapper(request) {
                    @Override
                    public Cookie[] getCookies() {
                        Cookie[] cookies = super.getCookies();
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Original cookies");
                            LogHandler.debugCookies(LOG, cookies);
                        }
                        if (cookies != null) {
                            return Arrays.stream(cookies).filter(cookie ->
                                    !cookie.getName().equalsIgnoreCase(JSESSIONID)).toArray(Cookie[]::new);
                        }
                        return null;
                    }
                };

                chain.doFilter(requestWrapper, servletResponse);
            }
        } else {
            chain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}

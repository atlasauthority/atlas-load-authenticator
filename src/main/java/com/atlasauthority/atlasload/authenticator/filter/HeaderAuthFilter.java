package com.atlasauthority.atlasload.authenticator.filter;

import static com.atlasauthority.atlasload.authenticator.api.AuthPluginHelper.JSESSIONID;
import static com.atlasauthority.atlasload.authenticator.api.AuthPluginHelper.USERNAME_CUSTOM_HEADER;

import java.io.IOException;
import java.net.URLDecoder;
import java.security.Principal;
import java.util.Arrays;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.atlasauthority.atlasload.authenticator.api.AuthPluginHelper;
import com.atlasauthority.atlasload.authenticator.utils.LogHandler;
import com.atlasauthority.atlasload.authenticator.utils.StateHolder;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.user.LoginEvent;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.seraph.auth.LoginReason;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard;

/**
 * @author Andrew Kharchenko
 */
public class HeaderAuthFilter implements Filter {
    private static final Logger LOG = LogHandler.getLogger(HeaderAuthFilter.class);

    @Inject
    @ComponentImport
    private AuthPluginHelper authPluginHelper;

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (authPluginHelper.filteringEnabled()) {
            boolean notWhitelisted = authPluginHelper.requestNotWhitelisted(request);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Request " + request.getRequestURI() + " is " + (notWhitelisted ? "NOT" : "") + " whitelisted");
            }
            if (notWhitelisted) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
        }

        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        String userName = request.getHeader(USERNAME_CUSTOM_HEADER);
        if (userName != null) {
            userName = URLDecoder.decode(userName, "utf-8");
            synchronized (userName) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Handling request for user: " + userName);
                    LogHandler.debugRequest(LOG, request);
                    LogHandler.debugCookies(LOG, request.getCookies());
                }
                ApplicationUser userByName = ComponentAccessor.getUserManager().getUserByName(userName);
                if (userByName != null) {
                    HttpSession session = request.getSession(false);
                    session = handleSessionAndJSessionId(request, (HttpServletResponse) servletResponse, session, userName);

                    ServletContext servletContext = session.getServletContext();
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("ServletContext: " + servletContext.toString());
                    }

                    if (servletContext != null) {
                        final SecurityConfig securityConfig = (SecurityConfig) servletContext.getAttribute("seraph_config");

                        ElevatedSecurityGuard elevatedSecurityGuard = securityConfig.getElevatedSecurityGuard();
                        String finalUserName = userName;
                        putPrincipalInSessionContext(request, () -> finalUserName);
                        authPluginHelper.getEventPublisher().publish(new LoginEvent(userByName));

                        elevatedSecurityGuard.onSuccessfulLoginAttempt(request, userName);

                        LoginReason.OK.stampRequestResponse(request, (HttpServletResponse) servletResponse);

                        authenticationContext.setLoggedInUser(userByName);
                        if (LOG.isDebugEnabled()) {
                            LOG.debug("-- Request: " + request.getRequestURI() + " should be authenticated now.");
                        }
                    }
                }
            }
        } else {
            LOG.debug("Username is null. Skipping handling.");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }

    private HttpSession handleSessionAndJSessionId(HttpServletRequest request, HttpServletResponse response,
                                                   HttpSession session, String userName) {
        String jsessionid = getCookie(request, JSESSIONID);
        HttpSession resultSession = session == null ? request.getSession() : session;
        if (resultSession == null) {
            resultSession = request.getSession(true);
        }
        if (jsessionid == null) {
            String storedSessionId = StateHolder.getSessionIdByUserName(userName);
            if (storedSessionId != null) {
                response.addCookie(new Cookie(JSESSIONID, storedSessionId));
            }
        } else {
            if (!resultSession.getId().equals(jsessionid)) {
                jsessionid = resultSession.getId();
            }
            StateHolder.saveSessionId(userName, jsessionid);
        }
        return resultSession;
    }

    private String getCookie(HttpServletRequest request, String cookieName) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            return Arrays.stream(cookies).filter(cookie -> cookie.getName().equalsIgnoreCase(cookieName))
                    .findFirst().orElse(new Cookie(cookieName, null)).getValue();
        }
        return null;
    }

    private void putPrincipalInSessionContext(HttpServletRequest httpServletRequest, Principal principal) {
        HttpSession httpSession = httpServletRequest.getSession();
        httpSession.setAttribute("seraph_defaultauthenticator_user", principal);
        httpSession.setAttribute("seraph_defaultauthenticator_logged_out_user", null);
    }

}

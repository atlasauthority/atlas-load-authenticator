package com.atlasauthority.atlasload.authenticator.utils;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * @author Andrew Kharchenko
 */
public class LogHandler {
    private static Map<Class, Logger> loggers = new HashMap<>();
    private static Map<Logger, Level> defaultLogLevels = new HashMap<>();

    public static Logger getLogger(Class clazz) {
        Logger logger = loggers.get(clazz);
        if (logger == null) {
            logger = Logger.getLogger(clazz);
            loggers.put(clazz, logger);
        }
        return logger;
    }

    public static void setDebugLogLevel() {
        if (defaultLogLevels.isEmpty()) {
            loggers.forEach((key, logger) -> {
                defaultLogLevels.put(logger, logger.getLevel());
                logger.setLevel(Level.DEBUG);
            });
        } else {
            loggers.forEach((key, logger) -> {
                logger.setLevel(Level.DEBUG);
            });
        }
    }

    public static void restoreLogLevel() {
        defaultLogLevels.forEach((logger, level) -> {
            logger.setLevel(level);
        });
    }

    public static void debugCookies(Logger logger, Cookie[] cookies) {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                logger.debug(cookieToString(cookie));
            }
        }
    }

    private static String cookieToString(Cookie cookie) {
        return "\t\t\tCookie: {" +
                "comment: '" + cookie.getComment() +
                "', domain: '" + cookie.getDomain() +
                "', name: '" + cookie.getName() +
                "', path: '" + cookie.getPath() +
                "', value: '" + cookie.getValue() +
                "', maxAge: '" + cookie.getMaxAge() +
                "', secure: '" + cookie.getSecure() +
                "', version: '" + cookie.getVersion() + "'}";
    }

    public static void debugRequest(Logger logger, HttpServletRequest request) {
        logger.debug("\t\t\tRequest: {" +
        "requestURI: '" + request.getRequestURI() +
        "', authType: '" + request.getAuthType() +
        "', contextPath: '" + request.getContextPath() +
        "', method: '" + request.getMethod() +
        "', pathInfo: '" + request.getPathInfo() +
                "', pathTranslated: '" + request.getPathTranslated() +
                "', queryString: '" + request.getQueryString() +
                "', remoteUser: '" + request.getRemoteUser() +
                "', requestedSessionId: '" + request.getRequestedSessionId() +
                "', servletPath: '" + request.getServletPath() +
                "', characterEncoding: '" + request.getCharacterEncoding() +
                "', contentType: '" + request.getContentType() +
                "', localAddr: '" + request.getLocalAddr() +
                "', localName: '" + request.getLocalName() +
                "', protocol: '" + request.getProtocol() +
                "', remoteAddr: '" + request.getRemoteAddr() +
                "', remoteHost: '" + request.getRemoteHost() +
                "', scheme: '" + request.getScheme() +
                "', serverName: '" + request.getServerName() +
                "', contentLength: '" + request.getContentLength() +
                "', localPort: '" + request.getLocalPort() +
                "', remotePort: '" + request.getRemotePort() +
                "', parameters: " + renderParameterMap(request.getParameterMap()) +
                ", headers: " + renderHeaders(request) +
                ", serverPort: '" + request.getServerPort() +  "'}");
    }

    private static String renderHeaders(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String name = headerNames.nextElement();
            sb.append(name).append(": ");
            sb.append(request.getHeader(name)).append(", ");
        }
        sb.append("}");
        return sb.toString();
    }

    private static String renderParameterMap(Map<String, String[]> params) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        params.forEach((key, values) -> {
            sb.append(key).append(": ");
            if (values != null) {
                sb.append("[");
                for (String value: values) {
                    sb.append(value).append(", ");
                }
                sb.append("]");
            } else {
                sb.append("null, ");
            }
        });
        sb.append("}");
        return sb.toString();
    }
}


























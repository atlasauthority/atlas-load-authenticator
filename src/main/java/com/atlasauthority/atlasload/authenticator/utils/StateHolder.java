package com.atlasauthority.atlasload.authenticator.utils;

import org.apache.commons.collections.map.LRUMap;

public class StateHolder {
    private static StateHolder instance;

    private LRUMap userToSessionId;

    private StateHolder() {
        userToSessionId = new LRUMap();
    }

    private static StateHolder getInstance() {
        if (instance == null) {
            instance = new StateHolder();
        }
        return instance;
    }

    public static String getSessionIdByUserName(String userName) {
        return (String) getInstance().userToSessionId.get(userName);
    }

    public static void saveSessionId(String userName, String sessionId) {
        getInstance().userToSessionId.put(userName, sessionId);
    }
}

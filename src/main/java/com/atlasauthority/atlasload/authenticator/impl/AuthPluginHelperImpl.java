package com.atlasauthority.atlasload.authenticator.impl;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.log4j.Logger;

import com.atlasauthority.atlasload.authenticator.api.AuthPluginHelper;
import com.atlasauthority.atlasload.authenticator.rest.model.PluginModel;
import com.atlasauthority.atlasload.authenticator.utils.LogHandler;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import inet.ipaddr.IPAddressString;
import inet.ipaddr.ipv4.IPv4Address;
import inet.ipaddr.ipv6.IPv6Address;

@ExportAsService({AuthPluginHelper.class})
@Named("authPluginHelper")
public class AuthPluginHelperImpl implements AuthPluginHelper {
    private static final Logger LOG = LogHandler.getLogger(AuthPluginHelperImpl.class);

    @ComponentImport
    private final ApplicationProperties applicationProperties;
    @JiraImport
    private EventPublisher eventPublisher;

    private PluginSettings pluginSettings;

    @Inject
    public AuthPluginHelperImpl(final ApplicationProperties applicationProperties,
                                EventPublisher eventPublisher,
                                PluginSettingsFactory pluginSettingsFactory) {
        this.eventPublisher = eventPublisher;
        this.applicationProperties = applicationProperties;
        pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    public String getName() {
        if (null != applicationProperties) {
            return "myComponent:" + applicationProperties.getDisplayName();
        }

        return "myComponent";
    }

    @Override
    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    @Override
    public boolean requestNotWhitelisted(HttpServletRequest request) {
        String whitelistedIPs = (String) pluginSettings.get(PluginModel.class.getName() + ".whitelistedIPs");
        if (whitelistedIPs == null || whitelistedIPs.isEmpty()) {
            return true;
        }
        String remoteAddr = request.getHeader("Remote_Addr");
        if (remoteAddr == null || remoteAddr.isEmpty()) {
            remoteAddr = request.getHeader("HTTP_X_FORWARDED_FOR");
            if (remoteAddr == null || remoteAddr.isEmpty()) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        if (remoteAddr == null || remoteAddr.isEmpty()) {
            return true;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("Remote address: " + remoteAddr);
        }

        try {
            SubnetUtils utils = new SubnetUtils(whitelistedIPs);

            IPAddressString ipAddressString = new IPAddressString(remoteAddr);
            if (ipAddressString.isIPv6()) {
                IPv6Address iPv6Address = ipAddressString.getAddress().toIPv6();
                if (iPv6Address.isIPv4Convertible()) {
                    IPv4Address iPv4Address = iPv6Address.toIPv4();
                    return !utils.getInfo().isInRange(iPv4Address.toNormalizedString());
                }
                return true;
            } else {
                return !utils.getInfo().isInRange(remoteAddr);
            }
        } catch (Exception e) {
            return true;
        }
    }

    @Override
    public boolean filteringEnabled() {
        String filteringEnabled = (String) pluginSettings.get(PluginModel.class.getName() + ".filteringEnabled");
        if (filteringEnabled == null) {
            filteringEnabled = "true";
        }
        return Boolean.valueOf(filteringEnabled);
    }
}
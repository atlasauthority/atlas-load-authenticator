package com.atlasauthority.atlasload.authenticator.api;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.event.api.EventPublisher;

public interface AuthPluginHelper {
    String USERNAME_CUSTOM_HEADER = "X-ALOADUSERNAME";
    String JSESSIONID = "JSESSIONID";

    String getName();

    EventPublisher getEventPublisher();

    boolean requestNotWhitelisted(HttpServletRequest request);
    boolean filteringEnabled();
}